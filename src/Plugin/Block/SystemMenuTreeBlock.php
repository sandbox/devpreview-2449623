<?php

/**
 * @file
 * Contains \Drupal\menu_tree_block\Plugin\Block\SystemMenuTreeBlock.
 */

namespace Drupal\menu_tree_block\Plugin\Block;

use Drupal\system\Plugin\Block\SystemMenuBlock;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Provides a Tree Menu block.
 *
 * @Block(
 *   id = "system_menu_tree_block",
 *   admin_label = @Translation("Menu"),
 *   category = @Translation("Menus"),
 *   deriver = "Drupal\menu_tree_block\Plugin\Derivative\SystemMenuTreeBlock"
 * )
 */
class SystemMenuTreeBlock extends SystemMenuBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu_name = $this->getDerivativeId();
    $parameters = new MenuTreeParameters;
    $active_trail = $this->menuActiveTrail->getActiveTrailIds($menu_name);
    $parameters->setActiveTrail($active_trail);

    // Adjust the menu tree parameters based on the block's configuration.
    $level = $this->configuration['level'];
    $depth = $this->configuration['depth'];
    $parameters->setMinDepth($level);
    // When the depth is configured to zero, there is no depth limit. When depth
    // is non-zero, it indicates the number of levels that must be displayed.
    // Hence this is a relative depth that we must convert to an actual
    // (absolute) depth, that may never exceed the maximum depth.
    if ($depth > 0) {
      $parameters->setMaxDepth(min($level + $depth - 1, $this->menuTree->maxDepth()));
    }

    $tree = $this->menuTree->load($menu_name, $parameters);
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $this->menuTree->transform($tree, $manipulators);
    return $this->menuTree->build($tree);
  }

}
