<?php

/**
 * @file
 * Contains \Drupal\menu_tree_block\Plugin\Derivative\SystemMenuTreeBlock.
 */

namespace Drupal\menu_tree_block\Plugin\Derivative;

use Drupal\system\Plugin\Derivative\SystemMenuBlock;

/**
 * Provides block plugin definitions for custom tree menus.
 *
 * @see \Drupal\menu_tree_block\Plugin\Block\SystemMenuTreeBlock
 */
class SystemMenuTreeBlock extends SystemMenuBlock {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (parent::getDerivativeDefinitions($base_plugin_definition) as $menu => $definition) {
      $this->derivatives[$menu]['admin_label'] .= ' (tree)';
    }
    return $this->derivatives;
  }

}
